from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import masuk_data,hitung_ip,reset
from .models import mark
# Create your tests here.
class ReminderUnitCase(TestCase):
    # test url
    def test_nilai_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
 
    # test fungsi yang digunakan dari views
    def test_nilai_using_index_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'landing.html')
    
    def test_can_reset(self):
        response = self.client.post('/hapus/')
        hitung_jumlah = mark.objects.all().count()
        self.assertEqual(hitung_jumlah,0)