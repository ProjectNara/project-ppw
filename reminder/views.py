from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect

from .forms import TodoForm
from .models import Todo
# Create your views here.

def index(request):
    listTodo = Todo.objects.all()
    form = TodoForm()
    listSize = listTodo.count()
    context = {'rForm':form, 'listTodo':listTodo, 'size':listSize}
    return render(request, 'index1.html', context)

def add(request):
    if request.method == 'POST':
        form = TodoForm(request.POST)
        if form.is_valid():
            form.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def delete(request, todo_id):
    todo = Todo.objects.get(pk=todo_id)
    todo.delete()

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def deleteAll(request):
    Todo.objects.all().delete()

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def about_us(request):
    listTodo = Todo.objects.all()
    form = TodoForm()
    listSize = listTodo.count()
    context = {'rForm':form, 'listTodo':listTodo, 'size':listSize}
    return render(request, 'about.html', context)
