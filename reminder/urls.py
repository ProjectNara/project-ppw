from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('add/', views.add, name='add'),
    path('delete/<todo_id>', views.delete, name='delete'),
    path('deleteAll/', views.deleteAll, name='deleteAll'),
    path('about-us/', views.about_us, name='about')
]