from django import forms
from django.forms import ModelForm
from .models import Todo

class TodoForm(ModelForm):
    class Meta:
        model = Todo
        fields = ['todo']
        widgets = { 
            'todo': forms.TextInput(attrs={
                'placeholder':'Add To-Do', 
                'style':'width:80%; background-color:#E5E5E5; border:0'
                }),
        }