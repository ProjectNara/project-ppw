from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, add, delete, deleteAll
from .models import Todo
# Create your tests here.
class ReminderUnitCase(TestCase):
    def setUp(self):
        Todo.objects.create(todo="todo1")
        Todo.objects.create(todo="todo2")

    # test url
    def test_reminder_url_is_exist(self):
        response = Client().get('/reminder/')
        self.assertEqual(response.status_code, 200)

    # test models
    def test_new_object_added_to_models(self):
        hitung_jumlah = Todo.objects.all().count()
        self.assertEqual(hitung_jumlah,2)

    # test fungsi yang digunakan dari views
    def test_reminder_using_index_template(self):
        response = Client().get('/reminder/')
        self.assertTemplateUsed(response, 'index1.html')

    def test_reminder_using_add_func(self):
        found = resolve('/reminder/add/')
        self.assertEqual(found.func, add)

    def test_reminder_using_delete_func(self):
        found = resolve('/reminder/delete/<todo_id>')
        self.assertEqual(found.func, delete)

    def test_reminder_using_deleteAll_func(self):
        found = resolve('/reminder/deleteAll/')
        self.assertEqual(found.func, deleteAll)

    def test_can_save_a_POST_request(self):
        response = self.client.post('/reminder/add/', data={'todo':'apaja'})
        hitung_jumlah = Todo.objects.all().count()
        self.assertEqual(hitung_jumlah,3)

    def test_can_delete_an_object(self):
        response = self.client.post('/reminder/delete/1')
        hitung_jumlah = Todo.objects.all().count()
        self.assertEqual(hitung_jumlah,1)

    def test_can_delete_all_objects(self):
        response = self.client.post('/reminder/deleteAll/')
        hitung_jumlah = Todo.objects.all().count()
        self.assertEqual(hitung_jumlah,0)

    # test kelengkapan html
    def test_landing_page_content_is_completed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Your To-Do List:', html_response)
        self.assertIn('delete all', html_response)
        self.assertIn('All To-Do List', html_response)
