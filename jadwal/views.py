from django.shortcuts import render
from django.contrib.sites import requests
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse
from django.urls import reverse

# Create your views here.


def index(request):
    return render(request, 'landing.html')


def logout(request):
    """Logs out user"""
    request.session.flush()
    return HttpResponseRedirect(reverse('jadwal:index'))
