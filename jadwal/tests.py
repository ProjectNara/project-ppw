from django.test import TestCase, Client
from django.urls import resolve
from jadwal.views import index
from jadwal.calviews import CalendarView

# Create your tests here.


class JadwalTest(TestCase):
    # testing whether the url exist or not
    def test_landing_url_is_exist(self):
        response = Client().get('')  # calling router if browser get
        self.assertEqual(response.status_code, 200)

    # Test whether the page is using appropriate template landing.html

    def test_landing_using_to_do_list_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'landing.html')

    # test to make sure path using index function

    def test_landing_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_calendar_using_func(self):
        response = Client().get('/calendar/')
        self.assertTemplateUsed(response, 'calendar.html')
