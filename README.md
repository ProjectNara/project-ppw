# Project PPW

Anggota:
1. Ariell Zaki Prabaswara Ariza
2. Nadia Adilah Rahimullah
3. Rabialco Argana
4. Ronaldi Tjaidianto

Link Heroku app: project-nara.herokuapp.com

## Kegunaan
Membantu mahasiswa Universitas Indonesia mengatur jadwal belajar dan perkuliahan. 
Selain itu akan membantu mahasiswa menentukan target IP mereka dengan adanya fitur
IP Kalkulator. Website kami juga akan membantu dalam pengingatan deadline dengan
adanya halaman Your Deadline. Kemudian juga ada widget To Do List untuk mengingatkan
tugas-tugasnya.
//NTAR DIAPUSS

## Fitur
1. Schedule Planner
2. Score Calculator
3. Your Deadline
4. To do widget

## Status Pipelines
[![pipeline status](https://gitlab.com/ProjectNara/project-ppw/badges/master/pipeline.svg)](https://gitlab.com/ProjectNara/project-ppw/commits/master)