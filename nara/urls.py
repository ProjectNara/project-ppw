"""nara URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
    3. Commented urls will be implemented in times
"""
from django.contrib import admin
from django.urls import path, include, re_path
from django.conf.urls import include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(('jadwal.urls', 'jadwal'), namespace='jadwal')),
    #     path('deadline/',include('deadline.urls')),
    path('nilai/',include('nilai.urls')),
    #     path('reminder/',include('reminder.urls'))
    path('deadline/',include('deadline.urls')),
    # path('admin/', admin.site.urls),
    # path('', include(('jadwal.urls', 'jadwal'), namespace='jadwal')),
    path('auth/', include('social_django.urls', namespace='social')),
    path('reminder/',include('reminder.urls'))
]
